/* $Id$ */

/* Copyright (C) 1998-99 Martin Baulig
   This file is part of LibGTop 1.0.

   Contributed by Martin Baulig <martin@home-of-linux.org>, April 1998.

   LibGTop is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

   LibGTop is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with LibGTop; see the file COPYING. If not, write to the
   Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.
*/

#include "examples.h"

#include <netinet/in.h>
#include <arpa/inet.h>

#ifndef PROFILE_COUNT
#define PROFILE_COUNT	1
#endif

int
main (int argc, char *argv [])
{
    glibtop_array array;
    glibtop_interface *interfaces;
    int i;

    init_examples ("interfaces", &argc, argv, NULL, PROFILE_COUNT);

    glibtop_init_r (&glibtop_global_server, 0, 0);

    memset (&array, 0, sizeof (glibtop_array));

    interfaces = glibtop_get_interface_names
	(&array, 0, 0, 0, GLIBTOP_INTERFACES_ALL |
	 GLIBTOP_INTERFACES_INCLUDE_LOGICAL);

    if (!interfaces) {
	glibtop_close ();
	exit (1);
    }

    for (i = 0; i < array.number; i++) {
	fprintf (stderr, "INTERFACE: |%s|\n", interfaces [i].name);
    }

    glibtop_close ();

    exit (0);
}
