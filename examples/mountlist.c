/* $Id$ */

/* Copyright (C) 1998-99 Martin Baulig
   This file is part of LibGTop 1.0.

   Contributed by Martin Baulig <martin@home-of-linux.org>, April 1998.

   LibGTop is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

   LibGTop is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with LibGTop; see the file COPYING. If not, write to the
   Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.
*/

#include "examples.h"

#ifndef PROFILE_COUNT
#define PROFILE_COUNT	1000
#endif

int
main (int argc, char *argv [])
{
    glibtop_fsusage fsusage;
    glibtop_mountlist mount_list;
    glibtop_mountentry *mount_entries;
    long c, index;

    init_examples ("mountlist", &argc, argv, NULL, PROFILE_COUNT);

    printf ("sbrk (0) = %p\n\n", sbrk (0));

    for (c = 0; c < PROFILE_COUNT; c++) {
	mount_entries = glibtop_get_mountlist (&mount_list, 1);

	glibtop_free (mount_entries);
    }

    printf ("sbrk (0) = %p\n\n", sbrk (0));

    mount_entries = glibtop_get_mountlist (&mount_list, 1);

    if (mount_entries == NULL)
	_exit (1);

    for (index = 0; index < mount_list.number; index++)
	printf ("Mount_Entry: %-30s %-10s %-20s\n",
		mount_entries [index].mountdir,
		mount_entries [index].type,
		mount_entries [index].devname);

    printf ("\n\n%-23s %9s %9s %9s %9s %9s\n\n",
	    "", "Blocks", "Free", "Avail", "Files", "Free");

    for (index = 0; index < mount_list.number; index++) {
	glibtop_get_fsusage (&fsusage,
			     mount_entries [index].mountdir);

	printf ("Usage: %-16s %9llu %9llu %9llu %9llu %9llu\n",
		mount_entries [index].mountdir,
		fsusage.blocks, fsusage.bfree,
		fsusage.bavail, fsusage.files,
		fsusage.ffree);
    }

    glibtop_free (mount_entries);

    printf ("\nsbrk (0) = %p\n\n", sbrk (0));

    glibtop_close ();

    exit (0);
}
