/* $Id$ */

/* Copyright (C) 1998-99 Martin Baulig
   This file is part of LibGTop 1.0.

   Contributed by Martin Baulig <martin@home-of-linux.org>, April 1998.

   LibGTop is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

   LibGTop is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with LibGTop; see the file COPYING. If not, write to the
   Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.
*/

#include "examples.h"

#include <netinet/in.h>
#include <arpa/inet.h>

#ifndef PROFILE_COUNT
#define PROFILE_COUNT	1
#endif

static void
output (const char *interface)
{
    glibtop_netinfo netinfo;
    glibtop_ifaddr *retval;
    glibtop_array array;
    char *if_flags_string;
    int i;

    retval = glibtop_get_netinfo (&array, &netinfo, interface,
				  GLIBTOP_TRANSPORT_ALL);

    if_flags_string = libgtop_if_flags (netinfo.if_flags);

    fprintf (stderr, "%s - (%s) - MTU:%ld\n\n", interface, if_flags_string,
	     (unsigned long) netinfo.mtu);

    for (i = 0; i < array.number; i++) {
	char *address_string, *subnet_string;
	struct in_addr addr, subnet;

	switch (retval [i].transport) {
	case GLIBTOP_TRANSPORT_IPV4:
	    memcpy (&addr.s_addr, retval [i].address,
		    sizeof (addr.s_addr));
	    memcpy (&subnet.s_addr, &retval [i].subnet,
		    sizeof (subnet.s_addr));

	    address_string = glibtop_strdup (inet_ntoa (addr));
	    subnet_string  = glibtop_strdup (inet_ntoa (subnet));

	    fprintf (stderr, "\tipv4:\t%s - %s\n", address_string,
		     subnet_string);

	    break;
	case GLIBTOP_TRANSPORT_IPV6:
	    address_string = libgtop_inet6_ntoa (retval [i].address,
						 retval [i].addr_len);

	    fprintf (stderr, "\tipv6:\t%s/%d\n", address_string,
		     retval [i].addr_len);

	    break;
	}
    }

    fprintf (stderr, "\n");
}

int
main (int argc, char *argv [])
{

    init_examples ("netinfo", &argc, argv, NULL, PROFILE_COUNT);

    glibtop_init_r (&glibtop_global_server, 0, 0);

    if (argc != 2)
	glibtop_error ("Usage: %s interface", argv [0]);

    output (argv [1]);

    glibtop_close ();

    exit (0);
}
