/* $Id$ */

/* Copyright (C) 1998-99 Martin Baulig
   This file is part of LibGTop 1.0.

   Contributed by Martin Baulig <martin@home-of-linux.org>, April 1998.

   LibGTop is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

   LibGTop is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with LibGTop; see the file COPYING. If not, write to the
   Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.
*/

#include "examples.h"

#ifndef PROFILE_COUNT
#define PROFILE_COUNT	1
#endif

int
main (int argc, char *argv [])
{
    glibtop_netload netload;

    init_examples ("netload", &argc, argv, NULL, PROFILE_COUNT);

    if (argc != 2)
	glibtop_error ("Usage: %s interface", argv [0]);
	
    glibtop_get_netload (&netload, argv [1], 0, 0);

    printf ("Network Load (0x%08lx):\n\n"
	    "\tCollisions:\t\t%ld\n\n"
	    "\tPackets In:\t\t%ld\n"
	    "\tPackets Out:\t\t%ld\n"
	    "\tPackets Total:\t\t%ld\n\n"
	    "\tBytes In:\t\t%ld\n"
	    "\tBytes Out:\t\t%ld\n"
	    "\tBytes Total:\t\t%ld\n\n"
	    "\tErrors In:\t\t%ld\n"
	    "\tErrors Out:\t\t%ld\n"
	    "\tErrors Total:\t\t%ld\n\n",
	    (unsigned long) netload.flags,
	    (unsigned long) netload.collisions,
	    (unsigned long) netload.packets_in,
	    (unsigned long) netload.packets_out,
	    (unsigned long) netload.packets_total,
	    (unsigned long) netload.bytes_in,
	    (unsigned long) netload.bytes_out,
	    (unsigned long) netload.bytes_total,
	    (unsigned long) netload.errors_in,
	    (unsigned long) netload.errors_out,
	    (unsigned long) netload.errors_total);

    glibtop_close ();

    exit (0);
}
