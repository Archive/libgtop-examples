/* $Id$ */

/* Copyright (C) 1998-99 Martin Baulig
   This file is part of LibGTop 1.0.

   Contributed by Martin Baulig <martin@home-of-linux.org>, April 1998.

   LibGTop is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

   LibGTop is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with LibGTop; see the file COPYING. If not, write to the
   Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.
*/

#include "examples.h"

#ifndef PROFILE_COUNT
#define PROFILE_COUNT	1
#endif

static int include_isdn = -1;
static int include_modem = -1;

struct poptOption options [] = {
    POPT_AUTOHELP
    { NULL, '\0', POPT_ARG_INCLUDE_TABLE, general_options, 0,
      N_("General Options"), NULL },
    { "isdn", 'i', POPT_ARG_INT, &include_isdn, 0,
      N_("Include ISDN statistics for interface NUMBER"),
      N_("NUMBER")
    },
    { "modem", 'm', POPT_ARG_INT, &include_modem, 0,
      N_("Include modem statistics for interface NUMBER"),
      N_("NUMBER")
    },
    { NULL, '\0', POPT_ARG_NONE, NULL, 0, NULL, NULL }
};

int
main (int argc, char *argv [])
{
    glibtop_ppp ppp;
    long c;

    init_examples ("ppp", &argc, argv, options, PROFILE_COUNT);

    if (include_isdn >= 0) {
	for (c = 0; c < PROFILE_COUNT; c++) {
	    glibtop_get_ppp (&ppp, include_isdn, TRUE);
	}

	printf ("PPP / ISDN   (0x%08lx): %lu, %lu, %lu\n",
		(unsigned long) ppp.flags,
		(unsigned long) ppp.state,
		(unsigned long) ppp.bytes_in,
		(unsigned long) ppp.bytes_out);

	printf ("\n");
    } else {
	include_modem = 0;
    }

    if (include_modem >= 0) {
	for (c = 0; c < PROFILE_COUNT; c++) {
	    glibtop_get_ppp (&ppp, include_modem, FALSE);
	}

	printf ("PPP / Modem  (0x%08lx): %lu, %lu, %lu\n",
		(unsigned long) ppp.flags,
		(unsigned long) ppp.state,
		(unsigned long) ppp.bytes_in,
		(unsigned long) ppp.bytes_out);

	printf ("\n");
    }

    glibtop_close ();

    exit (0);
}
