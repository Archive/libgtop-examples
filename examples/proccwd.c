/* $Id$ */

/* Copyright (C) 1998-99 Martin Baulig
   This file is part of LibGTop 1.0.

   Contributed by Martin Baulig <martin@home-of-linux.org>, April 1998.

   LibGTop is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

   LibGTop is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with LibGTop; see the file COPYING. If not, write to the
   Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.
*/

#include "examples.h"

#ifndef PROFILE_COUNT
#define PROFILE_COUNT	1
#endif

int
main (int argc, char *argv [])
{
    glibtop_proc_cwd proccwd;
    unsigned device, device_major, device_minor;
    char *cwd;
    pid_t pid;
    long i;

    init_examples ("proccwd", &argc, argv, NULL, PROFILE_COUNT);

    if ((argc != 2) || (sscanf (argv [1], "%d", (int *) &pid) != 1))
	glibtop_error ("Usage: %s pid", argv [0]);

    cwd = glibtop_get_proc_cwd (&proccwd, pid);

    device = (unsigned long) proccwd.device;
    device_minor = (device & 255);
    device_major = ((device >> 8) & 255);

    fprintf (stderr, "CWD (%d) - '%s' - %02x:%02x - %ld\n",
	     pid, cwd, device_major, device_minor, (unsigned long) proccwd.inode);

    glibtop_free (cwd);

    glibtop_close ();

    exit (0);
}
