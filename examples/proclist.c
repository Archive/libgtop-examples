/* $Id$ */

/* Copyright (C) 1998-99 Martin Baulig
   This file is part of LibGTop 1.0.

   Contributed by Martin Baulig <martin@home-of-linux.org>, April 1998.

   LibGTop is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

   LibGTop is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with LibGTop; see the file COPYING. If not, write to the
   Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.
*/

#include <config.h>
#include <locale.h>

#include <glibtop.h>
#include <glibtop/open.h>
#include <glibtop/close.h>
#include <glibtop/xmalloc.h>

#include <glibtop/union.h>
#include <glibtop/sysdeps.h>

#include <popt-gnome.h>

#if (defined HAVE_LIBINTL_H) || (defined HAVE_GETTEXT)
#include <libintl.h>
#else
#include <libgettext.h>
#endif

#ifndef _
#define _(String) dgettext (PACKAGE, String)
#define N_(String) (String)
#endif

static void
output (pid_t pid)
{
    glibtop_union data;

    glibtop_get_proc_state (&data.proc_state, pid);

    printf ("%5d %5d  %-20s %6lu %6lu\n",
	    (int) pid, data.proc_state.state, data.proc_state.cmd,
	    (unsigned long) data.proc_state.uid,
	    (unsigned long) data.proc_state.gid);
}

static int exclude_idle = 0;
static int exclude_system = 0;
static int exclude_notty = 0;

static long procselect_arg = 0;
static long procselect_which = 0;

static void
procselect_cb (poptContext context, enum poptCallbackReason reason,
	       const struct poptOption * opt, const char * arg,
	       const void * data)
{
    if (procselect_which) {
	printf (N_("Error on option %s: "
		   "May only set one process selection method.\n"),
		poptBadOption (context, 0));
	exit(1);
    }

    procselect_which = opt->val;
}

struct poptOption process_options [] = {
    { NULL, '\0', POPT_ARG_CALLBACK, procselect_cb, 0,
      NULL, NULL },
    { "uid", '\0', POPT_ARG_INT, &procselect_arg, GLIBTOP_KERN_PROC_UID,
      N_("Show processes with uid ARG"), NULL },
    { "ruid", '\0', POPT_ARG_INT, &procselect_arg, GLIBTOP_KERN_PROC_RUID,
      N_("Show processes with real uid ARG"), NULL },
    { "pgrp", '\0', POPT_ARG_INT, &procselect_arg, GLIBTOP_KERN_PROC_PGRP,
      N_("Show processes in process group ARG"), NULL },
    { "session", '\0', POPT_ARG_INT, &procselect_arg, GLIBTOP_KERN_PROC_SESSION,
      N_("Show processes in session ARG"), NULL },
    { "tty", '\0', POPT_ARG_INT, &procselect_arg, GLIBTOP_KERN_PROC_TTY,
      N_("Show processes with controlling tty ARG"), NULL },
    { "pid", '\0', POPT_ARG_INT, &procselect_arg, GLIBTOP_KERN_PROC_PID,
      N_("Show processes with pid ARG"), NULL },
    { "ppid", '\0', POPT_ARG_INT, &procselect_arg, GLIBTOP_KERN_PROC_PPID,
      N_("Show all children of process ARG"), NULL },
    { NULL, '\0', POPT_ARG_NONE, NULL, 0, NULL, NULL }
};

struct poptOption options [] = {
    POPT_AUTOHELP
    { NULL, '\0', POPT_ARG_INCLUDE_TABLE, process_options, 0,
      N_("Process Selectin"), NULL },
    { "idle", 'i', POPT_ARG_NONE, &exclude_idle, 0,
      N_("Exclude idle processes"), NULL },
    { "system", 's', POPT_ARG_NONE, &exclude_system, 0,
      N_("Exclude system processes"), NULL },
    { "notty", 't', POPT_ARG_NONE, &exclude_notty, 0,
      N_("Exclude processes without a controlling tty"), NULL },
    { NULL, '\0', POPT_ARG_NONE, NULL, 0, NULL, NULL }
};

int
main (int argc, char *argv [])
{
    glibtop_proclist proclist;
    unsigned *ptr, pid, i;

    poptContext context;
    int nextopt;

    u_int64_t which;

    setlocale (LC_ALL, "");
    bindtextdomain (PACKAGE, LIBGTOP_EXAMPLES_LOCALEDIR);
    textdomain (PACKAGE);
	
    glibtop_init ();

    context = poptGetContext ("proglist", argc, argv, options, 0);

    while ((nextopt = poptGetNextOpt (context)) > 0)
	/* do nothing */ ;

    if (nextopt != -1) {
	printf(N_("Error on option %s: %s.\n"
		  "Run '%s --help' to see a full list of "
		  "available command line options.\n"),
	       poptBadOption (context, 0),
	       poptStrerror (nextopt),
	       argv[0]);
	exit(1);
    }

    poptFreeContext (context);

    printf ("\n");

    which = procselect_which;

    if (exclude_idle)
	which |= GLIBTOP_EXCLUDE_IDLE;

    if (exclude_system)
	which |= GLIBTOP_EXCLUDE_SYSTEM;

    if (exclude_notty)
	which |= GLIBTOP_EXCLUDE_NOTTY;

    ptr = glibtop_get_proclist (&proclist, which, procselect_arg);

    printf ("Proclist     (0x%08lx): %lu, %lu, %lu\n",
	    (unsigned long) proclist.flags,
	    (unsigned long) proclist.number,
	    (unsigned long) proclist.size,
	    (unsigned long) proclist.total);

    if (!ptr) exit (1);

    printf ("\n");
    printf (N_(" PID  STATE  COMMAND                 UID    GID\n"));
    printf ("===============================================\n");

    for (i = 0; i < proclist.number; i++) {

	pid = ptr [i];
		
	output (pid);
    }

    glibtop_free (ptr);

    exit (0);
}
