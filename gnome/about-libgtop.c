/* $Id$ */

/* Copyright (C) 1998-99 Martin Baulig
   This file is part of LibGTop 1.0.

   Contributed by Martin Baulig <martin@home-of-linux.org>, April 1998.

   LibGTop is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

   LibGTop is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with LibGTop; see the file COPYING. If not, write to the
   Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.
*/

#include <gnome.h>
#include <examples.h>

#ifndef PROFILE_COUNT
#define PROFILE_COUNT	1
#endif

static void
show_about (void)
{
    GtkWidget *about;
    const gchar *authors[] = {
	"Martin Baulig <martin@home-of-linux.org>",
	NULL,
    };
	
    about = gnome_about_new (_("LibGTop"), LIBGTOP_VERSION,
			     /* copyright notice */
			     _("(C) 2000 the Free Software Foundation"),
			     authors,
			     /* another comments */
			     _("Portable system access library."),
			     NULL);

    gtk_signal_connect (GTK_OBJECT (about), "destroy",
			gtk_main_quit, NULL);

    gtk_widget_show (about);
	
    return;
}

int
main (int argc, char *argv [])
{
    init_examples ("first", &argc, argv, NULL, PROFILE_COUNT);

    gnome_init ("about-libgtop", VERSION, argc, argv);

    show_about ();

    gtk_main ();
	
    exit (0);
}
