/* $Id$ */

/* Copyright (C) 1998-99 Martin Baulig
   This file is part of LibGTop 1.0.

   Contributed by Martin Baulig <martin@home-of-linux.org>, April 1998.

   LibGTop is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

   LibGTop is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with LibGTop; see the file COPYING. If not, write to the
   Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.
*/

#include "examples.h"

#include <math.h>

extern long libgtop_profile_count;

void
print_data (void)
{
    glibtop_union data;
    long c;

    memset (&data, 0, sizeof (glibtop_union));

    for (c = 0; c < libgtop_profile_count; c++)
	glibtop_get_cpu (&data.cpu);

    printf ("CPU          (0x%08lx): %lu, %lu, %lu, %lu, %lu, %lu\n",
	    (unsigned long) data.cpu.flags,
	    (unsigned long) data.cpu.total,
	    (unsigned long) data.cpu.user,
	    (unsigned long) data.cpu.nice,
	    (unsigned long) data.cpu.sys,
	    (unsigned long) data.cpu.idle,
	    (unsigned long) data.cpu.frequency);

    memset (&data, 0, sizeof (glibtop_union));

    for (c = 0; c < libgtop_profile_count; c++)
	glibtop_get_mem (&data.mem);

    printf ("Memory       (0x%08lx): "
	    "%lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu\n",
	    (unsigned long) data.mem.flags,
	    (unsigned long) data.mem.total,
	    (unsigned long) data.mem.used,
	    (unsigned long) data.mem.free,
	    (unsigned long) data.mem.shared,
	    (unsigned long) data.mem.buffer,
	    (unsigned long) data.mem.cached,
	    (unsigned long) data.mem.user,
	    (unsigned long) data.mem.locked);

    memset (&data, 0, sizeof (glibtop_union));

    for (c = 0; c < libgtop_profile_count; c++)
	glibtop_get_swap (&data.swap);

    printf ("Swap         (0x%08lx): %lu, %lu, %lu, %lu, %lu\n",
	    (unsigned long) data.swap.flags,
	    (unsigned long) data.swap.total,
	    (unsigned long) data.swap.used,
	    (unsigned long) data.swap.free,
	    (unsigned long) data.swap.pagein,
	    (unsigned long) data.swap.pageout);

    memset (&data, 0, sizeof (glibtop_union));

    for (c = 0; c < libgtop_profile_count; c++)
	glibtop_get_uptime (&data.uptime);

    printf ("Uptime       (0x%08lx): %f, %f, %lu\n",
	    (unsigned long) data.uptime.flags,
	    data.uptime.uptime, data.uptime.idletime,
	    (unsigned long) data.uptime.boot_time);

    memset (&data, 0, sizeof (glibtop_union));

    for (c = 0; c < libgtop_profile_count; c++)
	glibtop_get_loadavg (&data.loadavg);

    printf ("Loadavg      (0x%08lx): %f, %f, %f - %lu, %lu, %lu\n",
	    (unsigned long) data.loadavg.flags,
	    (double) data.loadavg.loadavg [0],
	    (double) data.loadavg.loadavg [1],
	    (double) data.loadavg.loadavg [2],
	    (unsigned long) data.loadavg.nr_running,
	    (unsigned long) data.loadavg.nr_tasks,
	    (unsigned long) data.loadavg.last_pid);

    memset (&data, 0, sizeof (glibtop_union));

    for (c = 0; c < libgtop_profile_count; c++)
	glibtop_get_shm_limits (&data.shm_limits);
	
    printf ("Shm Limits   (0x%08lx): %lu, %lu, %lu, %lu, %lu\n",
	    (unsigned long) data.shm_limits.flags,
	    (unsigned long) data.shm_limits.shmmax,
	    (unsigned long) data.shm_limits.shmmin,
	    (unsigned long) data.shm_limits.shmmni,
	    (unsigned long) data.shm_limits.shmseg,
	    (unsigned long) data.shm_limits.shmall);

    memset (&data, 0, sizeof (glibtop_union));

    for (c = 0; c < libgtop_profile_count; c++)
	glibtop_get_msg_limits (&data.msg_limits);

    printf ("Msg Limits   (0x%08lx): %lu, %lu, %lu, %lu, %lu, %lu, %lu\n",
	    (unsigned long) data.msg_limits.flags,
	    (unsigned long) data.msg_limits.msgpool,
	    (unsigned long) data.msg_limits.msgmap,
	    (unsigned long) data.msg_limits.msgmax,
	    (unsigned long) data.msg_limits.msgmnb,
	    (unsigned long) data.msg_limits.msgmni,
	    (unsigned long) data.msg_limits.msgssz,
	    (unsigned long)  data.msg_limits.msgtql);

    memset (&data, 0, sizeof (glibtop_union));

    for (c = 0; c < libgtop_profile_count; c++)
	glibtop_get_sem_limits (&data.sem_limits);
	
    printf ("Sem Limits   (0x%08lx): "
	    "%lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu\n",
	    (unsigned long) data.sem_limits.flags,
	    (unsigned long) data.sem_limits.semmap,
	    (unsigned long) data.sem_limits.semmni,
	    (unsigned long) data.sem_limits.semmns,
	    (unsigned long) data.sem_limits.semmnu,
	    (unsigned long) data.sem_limits.semmsl,
	    (unsigned long) data.sem_limits.semopm,
	    (unsigned long) data.sem_limits.semume,
	    (unsigned long) data.sem_limits.semusz,
	    (unsigned long) data.sem_limits.semvmx,
	    (unsigned long) data.sem_limits.semaem);

    printf ("\n");

    memset (&data, 0, sizeof (glibtop_union));

    for (c = 0; c < libgtop_profile_count; c++)
	glibtop_get_ppp (&data.ppp, 0, TRUE);

    printf ("ISDN         (0x%08lx): %lu, %lu, %lu\n",
	    (unsigned long) data.ppp.flags,
	    (unsigned long) data.ppp.state,
	    (unsigned long) data.ppp.bytes_in,
	    (unsigned long) data.ppp.bytes_out);

    printf ("\n");
}

void
print_sysdeps (void)
{
    glibtop_sysdeps sysdeps;

    memset (&sysdeps, 0, sizeof (glibtop_sysdeps));

    glibtop_get_sysdeps (&sysdeps);

    printf ("Sysdeps      (0x%08lx): %lu, %lu, %lu, %lu, %lu, "
	    "%lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, %lu, "
	    "%lu, %lu, %lu, %lu, %lu, %lu\n",
	    (unsigned long) sysdeps.flags,
	    (unsigned long) sysdeps.cpu,
	    (unsigned long) sysdeps.mem,
	    (unsigned long) sysdeps.swap,
	    (unsigned long) sysdeps.uptime,
	    (unsigned long) sysdeps.loadavg,
	    (unsigned long) sysdeps.shm_limits,
	    (unsigned long) sysdeps.msg_limits,
	    (unsigned long) sysdeps.sem_limits,
	    (unsigned long) sysdeps.proclist,
	    (unsigned long) sysdeps.proc_state,
	    (unsigned long) sysdeps.proc_uid,
	    (unsigned long) sysdeps.proc_mem,
	    (unsigned long) sysdeps.proc_time,
	    (unsigned long) sysdeps.proc_signal,
	    (unsigned long) sysdeps.proc_kernel,
	    (unsigned long) sysdeps.proc_segment,
	    (unsigned long) sysdeps.proc_args,
	    (unsigned long) sysdeps.proc_map,
	    (unsigned long) sysdeps.mountlist,
	    (unsigned long) sysdeps.fsusage,
	    (unsigned long) sysdeps.netload,
	    (unsigned long) sysdeps.ppp);

    printf ("\n");
}

void
print_proclist (void)
{
    GArray *proclist;
    int i;

    proclist = glibtop_get_proclist_as_array (0, 0);
	
    if (proclist) {
	printf ("\nProcess: ");
	for (i = 0; i < proclist->len; i++)
	    printf ("%s%u", i ? ", " : "",
		    g_array_index (proclist, guint, i));
	printf ("\n");

	g_array_free (proclist, TRUE);
    }
}

void
print_proc_data (pid_t pid)
{
    glibtop_union data;
    GPtrArray *args;
    unsigned i;

#if HAVE_LIBGTOP_SMP
    unsigned long total;
    double p_total, p_utime, p_stime;
    double b_total, b_utime, b_stime;
    double s_total, s_utime, s_stime;
    double my_utime, my_stime;
    int ncpu;
#endif

    printf ("\n");
		
    glibtop_get_proc_state (&data.proc_state, pid);

#if LIBGTOP_VERSION_CODE > 1001000		
    printf ("Proc_State   PID  %5d (0x%08lx): '%s', %u - "
	    "%u, %u, %u, %u - %u, %u, %u\n", (int) pid,
	    (unsigned long) data.proc_state.flags,
	    data.proc_state.cmd, data.proc_state.state,
	    data.proc_state.uid, data.proc_state.gid,
	    data.proc_state.ruid, data.proc_state.rgid,
	    data.proc_state.has_cpu, data.proc_state.processor,
	    data.proc_state.last_processor);
#else
    printf ("Proc_State   PID  %5d (0x%08lx): "
	    "'%s', %c, %lu, %lu\n", (int) pid,
	    (unsigned long) data.proc_state.flags,
	    data.proc_state.cmd, data.proc_state.state,
	    (unsigned long) data.proc_state.uid,
	    (unsigned long) data.proc_state.gid);
#endif
		
    glibtop_get_proc_uid (&data.proc_uid, pid);
	
#if LIBGTOP_VERSION_CODE > 1001000	
    printf ("Proc_Uid     PID  %5d (0x%08lx): "
	    "%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d %d - %d",
	    (int) pid, (unsigned long) data.proc_uid.flags,
	    data.proc_uid.uid, data.proc_uid.euid,
	    data.proc_uid.gid, data.proc_uid.egid,
	    data.proc_uid.suid, data.proc_uid.sgid,
	    data.proc_uid.fsuid, data.proc_uid.fsgid,
	    data.proc_uid.pid, data.proc_uid.ppid,
	    data.proc_uid.pgrp, data.proc_uid.session,
	    data.proc_uid.tty, data.proc_uid.tpgid,
	    data.proc_uid.priority, data.proc_uid.nice,
	    data.proc_uid.ngroups);
		
    if (data.proc_uid.ngroups) {
	for (i = 0; i < data.proc_uid.ngroups; i++) {
	    if (i)
		printf (" %d", data.proc_uid.groups [i]);
	    else
		printf (" (%d", data.proc_uid.groups [i]);
	}
	printf (")");
    }

    printf ("\n");
#else
    printf ("Proc_Uid     PID  %5d (0x%08lx): "
	    "%d %d %d %d %d %d %d %d %d %d %d %d\n", (int) pid,
	    (unsigned long) data.proc_uid.flags,
	    data.proc_uid.uid, data.proc_uid.euid,
	    data.proc_uid.gid, data.proc_uid.egid,
	    data.proc_uid.pid, data.proc_uid.ppid,
	    data.proc_uid.pgrp, data.proc_uid.session,
	    data.proc_uid.tty, data.proc_uid.tpgid,
	    data.proc_uid.priority, data.proc_uid.nice);
#endif

    glibtop_get_proc_mem (&data.proc_mem, pid);
		
    printf ("Proc_Mem     PID  %5d (0x%08lx): "
	    "%lu %lu %lu %lu %lu %lu\n", (int) pid,
	    (unsigned long) data.proc_mem.flags,
	    (unsigned long) data.proc_mem.size,
	    (unsigned long) data.proc_mem.vsize,
	    (unsigned long) data.proc_mem.resident,
	    (unsigned long) data.proc_mem.share,
	    (unsigned long) data.proc_mem.rss,
	    (unsigned long) data.proc_mem.rss_rlim);
		
    glibtop_get_proc_segment (&data.proc_segment, pid);

#if LIBGTOP_VERSION_CODE > 1001000
    printf ("Proc_Segment PID  %5d (0x%08lx): "
	    "%lu %lu %lu %lu %lu 0x%lx 0x%lx 0x%lx "
	    "0x%lx 0x%lx 0x%lx 0x%lx 0x%lx "
	    "0x%lx 0x%lx 0x%lx 0x%lx\n", (int) pid,
	    (unsigned long) data.proc_segment.flags,
	    (unsigned long) data.proc_segment.text_rss,
	    (unsigned long) data.proc_segment.shlib_rss,
	    (unsigned long) data.proc_segment.data_rss,
	    (unsigned long) data.proc_segment.stack_rss,
	    (unsigned long) data.proc_segment.dirty_size,
	    (unsigned long) data.proc_segment.start_code,
	    (unsigned long) data.proc_segment.end_code,
	    (unsigned long) data.proc_segment.start_data,
	    (unsigned long) data.proc_segment.end_data,
	    (unsigned long) data.proc_segment.start_brk,
	    (unsigned long) data.proc_segment.end_brk,
	    (unsigned long) data.proc_segment.start_stack,
	    (unsigned long) data.proc_segment.start_mmap,
	    (unsigned long) data.proc_segment.arg_start,
	    (unsigned long) data.proc_segment.arg_end,
	    (unsigned long) data.proc_segment.env_start,
	    (unsigned long) data.proc_segment.env_end);
#else
    printf ("Proc_Segment PID  %5d (0x%08lx): "
	    "%lu %lu %lu %lu %lu %lu %lu %lu\n", (int) pid,
	    (unsigned long) data.proc_segment.flags,
	    (unsigned long) data.proc_segment.text_rss,
	    (unsigned long) data.proc_segment.shlib_rss,
	    (unsigned long) data.proc_segment.data_rss,
	    (unsigned long) data.proc_segment.stack_rss,
	    (unsigned long) data.proc_segment.dirty_size,
	    (unsigned long) data.proc_segment.start_code,
	    (unsigned long) data.proc_segment.end_code,
	    (unsigned long) data.proc_segment.start_stack);
#endif

    glibtop_get_proc_time (&data.proc_time, pid);
		
    printf ("Proc_Time    PID  %5d (0x%08lx): "
	    "%lu %lu %lu %lu %lu %lu %lu %lu %lu\n", (int) pid,
	    (unsigned long) data.proc_time.flags,
	    (unsigned long) data.proc_time.start_time,
	    (unsigned long) data.proc_time.rtime,
	    (unsigned long) data.proc_time.utime,
	    (unsigned long) data.proc_time.stime,
	    (unsigned long) data.proc_time.cutime,
	    (unsigned long) data.proc_time.cstime,
	    (unsigned long) data.proc_time.timeout,
	    (unsigned long) data.proc_time.it_real_value,
	    (unsigned long) data.proc_time.frequency);

    glibtop_get_proc_signal (&data.proc_signal, pid);
	
#if LIBGTOP_VERSION_CODE > 1001000
    printf ("Proc_Signal  PID  %5d (0x%08lx): "
	    "%lx %lx %lx %lx %lx %lx %lx %lx\n", (int) pid,
	    (unsigned long) data.proc_signal.flags,
	    (unsigned long) data.proc_signal.signal [0],
	    (unsigned long) data.proc_signal.signal [1],
	    (unsigned long) data.proc_signal.blocked [0],
	    (unsigned long) data.proc_signal.blocked [1],
	    (unsigned long) data.proc_signal.sigignore [0],
	    (unsigned long) data.proc_signal.sigignore [1],
	    (unsigned long) data.proc_signal.sigcatch [0],
	    (unsigned long) data.proc_signal.sigcatch [1]);
#else
    printf ("Proc_Signal  PID  %5d (0x%08lx): "
	    "%lu %lu %lu %lu\n", (int) pid,
	    (unsigned long) data.proc_signal.flags,
	    (unsigned long) data.proc_signal.signal,
	    (unsigned long) data.proc_signal.blocked,
	    (unsigned long) data.proc_signal.sigignore,
	    (unsigned long) data.proc_signal.sigcatch);
#endif

    glibtop_get_proc_kernel (&data.proc_kernel, pid);

    printf ("Proc_Kernel  PID  %5d (0x%08lx): "
	    "%lu %lu %lu %lu %lu 0x%lx 0x%lx 0x%lx (%s)\n", (int) pid,
	    (unsigned long) data.proc_kernel.flags,
	    (unsigned long) data.proc_kernel.k_flags,
	    (unsigned long) data.proc_kernel.min_flt,
	    (unsigned long) data.proc_kernel.maj_flt,
	    (unsigned long) data.proc_kernel.cmin_flt,
	    (unsigned long) data.proc_kernel.cmaj_flt,
	    (unsigned long) data.proc_kernel.kstk_esp,
	    (unsigned long) data.proc_kernel.kstk_eip,
	    (unsigned long) data.proc_kernel.nwchan,
	    data.proc_kernel.wchan);

    printf ("\n");

    args = glibtop_get_proc_args_as_array_l (glibtop_global_server, pid);

    if (args) {
	for (i = 0; i < args->len; i++) {
	    printf ("Argument %2d - |%s|\n", i,
		    (char *) args->pdata [i]);
	}

	g_ptr_array_free (args, TRUE);

	printf ("\n");
    }

#if HAVE_LIBGTOP_SMP
    ncpu = glibtop_global_server->ncpu;

    glibtop_get_proc_time (&data.proc_time, pid);
		
    total = (unsigned long) data.proc_time.utime +
	(unsigned long) data.proc_time.stime;

    p_total = total ? (double) total : 1.0;

    p_utime = (double) data.proc_time.utime * 100.0 / p_total;
    p_stime = (double) data.proc_time.stime * 100.0 / p_total;

    b_total = p_total / ncpu;
    b_utime = (double) data.proc_time.utime / ncpu;
    b_stime = (double) data.proc_time.stime / ncpu;

    s_total = 0.0; s_utime = 0.0; s_stime = 0.0;

    printf ("Proc_Time    PID  %5d (0x%08lx): %12lu   %12lu   %12lu\n", (int) pid,
	    (unsigned long) data.proc_time.flags, total,
	    (unsigned long) data.proc_time.utime,
	    (unsigned long) data.proc_time.stime);

    for (i = 0; i < ncpu; i++) {
	unsigned long this_total;

	this_total = (unsigned long) data.proc_time.xcpu_utime [i] +
	    (unsigned long) data.proc_time.xcpu_stime [i];

	printf ("CPU %3d      PID  %5d (0x%08lx): %12lu   %12lu   %12lu\n", i,
		(int) pid, (unsigned long) data.proc_time.flags, this_total,
		(unsigned long) data.proc_time.xcpu_utime [i],
		(unsigned long) data.proc_time.xcpu_stime [i]);

	s_total += fabs (((double) this_total) - b_total);
	s_utime += fabs (((double) data.proc_time.xcpu_utime [i]) - b_utime);
	s_stime += fabs (((double) data.proc_time.xcpu_stime [i]) - b_stime);
    }

    printf ("\n");

    printf ("Proc_Time    PID  %5d (0x%08lx): %12.3f   %12.3f   %12.3f\n", (int) pid,
	    (unsigned long) data.proc_time.flags, 100.0, p_utime, p_stime);

    for (i = 0; i < ncpu; i++) {
	double this_p_total, this_p_utime, this_p_stime;
	unsigned long this_total;

	this_total = (unsigned long) data.proc_time.xcpu_utime [i] +
	    (unsigned long) data.proc_time.xcpu_stime [i];

	this_p_total = (double) this_total * 100.0 / p_total;

	this_p_utime = (double) data.proc_time.xcpu_utime [i] * 100.0 / p_total;
	this_p_stime = (double) data.proc_time.xcpu_stime [i] * 100.0 / p_total;

	printf ("CPU %3d      PID  %5d (0x%08lx): %12.3f   %12.3f   %12.3f\n", i,
		(int) pid, (unsigned long) data.proc_time.flags,
		this_p_total, this_p_utime, this_p_stime);
    }

    printf ("\n");

    my_utime = (unsigned long) data.proc_time.utime ?
	(double) data.proc_time.utime : 1.0;
    my_stime = (unsigned long) data.proc_time.stime ?
	(double) data.proc_time.stime : 1.0;

    printf ("SPIN: %31s %12.3f   %12.3f   %12.3f\n", "", s_total * 100.0 / p_total,
	    s_utime * 100.0 / my_utime, s_stime * 100.0 / my_stime);

    printf ("\n");
#endif
}
