/* $Id$ */

/* Copyright (C) 1998-99 Martin Baulig
   This file is part of LibGTop 1.0.

   Contributed by Martin Baulig <martin@home-of-linux.org>, April 1998.

   LibGTop is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

   LibGTop is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with LibGTop; see the file COPYING. If not, write to the
   Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.
*/

#ifndef __LIBGTOP_EXAMPLES_H__
#define __LIBGTOP_EXAMPLES_H__ 1

#include <config.h>
#include <locale.h>

#include <glibtop.h>
#include <glibtop/open.h>
#include <glibtop/close.h>
#include <glibtop/xmalloc.h>

#include <glibtop/parameter.h>

#include <glibtop/union.h>
#include <glibtop/sysdeps.h>

#include <glibtop/glib-arrays.h>

#include <sys/resource.h>

#include <popt-gnome.h>

BEGIN_LIBGTOP_DECLS

#if (defined HAVE_LIBINTL_H) || (defined HAVE_GETTEXT)
#include <libintl.h>
#else
#include <libgettext.h>
#endif

#ifndef _
#define _(String) dgettext (LIBGTOP_PACKAGE, String)
#define N_(String) (String)
#endif

extern long libgtop_profile_count;
extern struct poptOption general_options [];

void
init_examples (const char *program_name, int *argc, char **argv,
	       struct poptOption *extra_options, long profile_count);

void print_libgtop_parameters (glibtop *server);

void print_data (void);

void print_sysdeps (void);

void print_proclist (void);

void print_proc_data (pid_t);

gchar *
libgtop_inet6_ntoa (u_int8_t *, u_int8_t);

gchar *
libgtop_if_flags (u_int64_t);

END_LIBGTOP_DECLS

#endif
