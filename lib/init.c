/* $Id$ */

/* Copyright (C) 1998-99 Martin Baulig
   This file is part of LibGTop 1.0.

   Contributed by Martin Baulig <martin@home-of-linux.org>, April 1998.

   LibGTop is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

   LibGTop is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with LibGTop; see the file COPYING. If not, write to the
   Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.
*/

#include "examples.h"

long libgtop_profile_count = 0;
static int show_parameters = 0;
static char *error_method_option = NULL;
static int error_method = 0;

static void
error_option_cb (poptContext context, enum poptCallbackReason reason,
		 const struct poptOption * opt, const char * arg,
		 const void * data)
{
    if (strcmp (opt->longName, "error-method"))
	return;

    fprintf (stderr, "TEST: |%s|\n", arg);

    if (!strcmp (arg, "ignore"))
	error_method = GLIBTOP_ERROR_METHOD_IGNORE;
    else if (!strcmp (arg, "once"))
	error_method = GLIBTOP_ERROR_METHOD_WARN_ONCE;
    else if (!strcmp (arg, "warn"))
	error_method = GLIBTOP_ERROR_METHOD_WARN;
    else if (!strcmp (arg, "abort"))
	error_method = GLIBTOP_ERROR_METHOD_ABORT;
    else {
	printf (N_("Error on option %s: "
		   "No such error method ``%s''.\n"),
		poptBadOption (context, 0), arg);

	exit(1);
    }
}

struct poptOption error_options [] = {
    { NULL, '\0', POPT_ARG_CALLBACK, error_option_cb, 0,
      NULL, NULL },
    { "error-method", 0, POPT_ARG_STRING, &error_method_option, 0,
      N_("LibGTop error method\n\n(one of ignore, once, warn or abort)"), NULL
    },
    { NULL, '\0', POPT_ARG_NONE, NULL, 0, NULL, NULL }
};

struct poptOption general_options [] = {
    POPT_AUTOHELP
    { NULL, '\0', POPT_ARG_INCLUDE_TABLE, error_options, 0,
      N_("LibGTop Error Handling"), NULL },
    { "profile-count", 0, POPT_ARG_LONG, &libgtop_profile_count, 0,
      N_("Call all LibGTop functions COUNT times"), N_("COUNT")
    },
    { "show-parameters", 0, POPT_ARG_NONE, &show_parameters, 0,
      N_("Show LibGTop parameters"), NULL
    },
    { NULL, '\0', POPT_ARG_NONE, NULL, 0, NULL, NULL }
};

void
init_examples (const char *program_name, int *argc, char **argv,
	       struct poptOption *options, long profile_count)
{
    poptContext context;
    struct poptOption *opt_table;
    int nextopt;

    setlocale (LC_ALL, "");
    bindtextdomain (PACKAGE, LIBGTOP_EXAMPLES_LOCALEDIR);
    textdomain (PACKAGE);
	
    glibtop_init_r (&glibtop_global_server, 0, 0);

    libgtop_profile_count = profile_count;

    opt_table = options ? options : general_options;
    context = poptGetContext (program_name, *argc, argv, opt_table, 0);

    while ((nextopt = poptGetNextOpt (context)) > 0)
	/* do nothing */ ;

    if (nextopt != -1) {
	printf(N_("Error on option %s: %s.\n"
		  "Run '%s --help' to see a full list of "
		  "available command line options.\n"),
	       poptBadOption (context, 0),
	       poptStrerror (nextopt),
	       argv [0]);
	exit(1);
    }

    poptFreeContext (context);

    if (error_method)
	glibtop_set_parameter_l (glibtop_global_server,
				 GLIBTOP_PARAM_ERROR_METHOD,
				 &error_method, sizeof (error_method));

    if (show_parameters) {
	print_libgtop_parameters (glibtop_global_server);
	exit (0);
    }
}

