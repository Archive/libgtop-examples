/* $Id$ */

/* Copyright (C) 1998-99 Martin Baulig
   This file is part of LibGTop 1.0.

   Contributed by Martin Baulig <martin@home-of-linux.org>, April 1998.

   LibGTop is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

   LibGTop is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with LibGTop; see the file COPYING. If not, write to the
   Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.
*/

#include "examples.h"

#include <arpa/inet.h>

gchar *
libgtop_if_flags (u_int64_t if_flags)
{
    char buffer [BUFSIZ];

    buffer [0] = '\0';
    if (if_flags & (1L << GLIBTOP_IF_FLAGS_UP))
	strcat (buffer, ",UP");
    if (if_flags & (1L << GLIBTOP_IF_FLAGS_BROADCAST))
	strcat (buffer, ",BROADCAST");
    if (if_flags & (1L << GLIBTOP_IF_FLAGS_DEBUG))
	strcat (buffer, ",DEBUG");
    if (if_flags & (1L << GLIBTOP_IF_FLAGS_LOOPBACK))
	strcat (buffer, ",LOOPBACK");
    if (if_flags & (1L << GLIBTOP_IF_FLAGS_POINTOPOINT))
	strcat (buffer, ",POINTOPOINT");
    if (if_flags & (1L << GLIBTOP_IF_FLAGS_RUNNING))
	strcat (buffer, ",RUNNING");
    if (if_flags & (1L << GLIBTOP_IF_FLAGS_NOARP))
	strcat (buffer, ",NOARP");
    if (if_flags & (1L << GLIBTOP_IF_FLAGS_PROMISC))
	strcat (buffer, ",PROMISC");
    if (if_flags & (1L << GLIBTOP_IF_FLAGS_ALLMULTI))
	strcat (buffer, ",ALLMULTI");
    if (if_flags & (1L << GLIBTOP_IF_FLAGS_OACTIVE))
	strcat (buffer, ",OACTIVE");
    if (if_flags & (1L << GLIBTOP_IF_FLAGS_SIMPLEX))
	strcat (buffer, ",SIMPLEX");
    if (if_flags & (1L << GLIBTOP_IF_FLAGS_LINK0))
	strcat (buffer, ",LINK0");
    if (if_flags & (1L << GLIBTOP_IF_FLAGS_LINK1))
	strcat (buffer, ",LINK1");
    if (if_flags & (1L << GLIBTOP_IF_FLAGS_LINK2))
	strcat (buffer, ",LINK2");
    if (if_flags & (1L << GLIBTOP_IF_FLAGS_ALTPHYS))
	strcat (buffer, ",ALTPHYS");
    if (if_flags & (1L << GLIBTOP_IF_FLAGS_MULTICAST))
	strcat (buffer, ",MULTICAST");

    if (buffer [0] == ',')
	return g_strdup (buffer+1);
    else
	return g_strdup (buffer);
}
