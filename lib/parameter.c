/* $Id$ */

/* Copyright (C) 1998-99 Martin Baulig
   This file is part of LibGTop 1.0.

   Contributed by Martin Baulig <martin@home-of-linux.org>, April 1998.

   LibGTop is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License,
   or (at your option) any later version.

   LibGTop is distributed in the hope that it will be useful, but WITHOUT
   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
   for more details.

   You should have received a copy of the GNU General Public License
   along with LibGTop; see the file COPYING. If not, write to the
   Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.
*/

#include "examples.h"

void
print_libgtop_parameters (glibtop *server)
{
    unsigned error_method, ncpu;
    u_int64_t features, os_version_code;
    gchar *error_method_string;
    int ret;

    fprintf (stderr, "LibGTop Parameters:\n------------------\n\n");

    ret = glibtop_get_parameter_l (server, GLIBTOP_PARAM_ERROR_METHOD,
				   &error_method, sizeof (error_method));

    switch (error_method) {
    case GLIBTOP_ERROR_METHOD_IGNORE:
	error_method_string = "ignore";
	break;
    case GLIBTOP_ERROR_METHOD_WARN_ONCE:
	error_method_string = "warn_once";
	break;
    case GLIBTOP_ERROR_METHOD_WARN:
	error_method_string = "warn";
	break;
    case GLIBTOP_ERROR_METHOD_ABORT:
	error_method_string = "abort";
	break;
    default:
	error_method_string = "unknown";
	break;
    }

    fprintf (stderr, "\tError Method:\t\t%s\n\n", error_method_string);

    ret = glibtop_get_parameter_l (server, GLIBTOP_PARAM_FEATURES,
				   &features, sizeof (features));

    fprintf (stderr, "\tFeatures:\t\t%lu\n\n", (unsigned long) features);

    ret = glibtop_get_parameter_l (server, GLIBTOP_PARAM_NCPU,
				   &ncpu, sizeof (ncpu));

    fprintf (stderr, "\tNumber of CPUs:\t\t%u\n\n", ncpu);

    ret = glibtop_get_parameter_l (server, GLIBTOP_PARAM_OS_VERSION_CODE,
				   &os_version_code, sizeof (os_version_code));

    fprintf (stderr, "\tOS Version Code:\t%lu\n\n",
	     (unsigned long) os_version_code);


}
